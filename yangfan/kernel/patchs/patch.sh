#!/bin/bash

set -e

maindir=$(pwd)
path_patch=${maindir}/device/board/isoftstone/yangfan/kernel/patchs

echo "++++++++++++++++++++++++++++++++++++++++"

patch_list=(
    applications/standard/hap                 applications-standard-hap                  001-applications-standard-hap.patch                
    base/startup/init                         base-startup-init                          001-base-startup-init.patch
    build                                     build                                      001-build.patch
    foundation/graphic/graphic_2d             foundation-graphic-graphic_2d              001-foundation-graphic-graphic_2d.patch
    device/soc/rockchip                       device-soc-rockchip                        001-device-soc-rockchip.patch
    kernel/linux/config                       rk3399_standard_defconfig                  001-rk3399_standard_defconfig.patch
    kernel/linux/patches                      kernel_patch                               001-kernel_patch.patch
    foundation/multimodalinput/input          foundation-multimodalinput-input           001-foundation-multimodalinput-input.patch
    drivers/hdf_core                          drivers-hdf_core                           001-drivers-hdf_core.patch
    foundation/window/window_manager          foundation-window-window_manager           001-foundation-window-window_manager.patch
)

function apply_rk3399_patch()
{
    for ((i=0; i<${#patch_list[*]}; i+=3))
    do
       echo "patch for ${patch_list[$i]} start"
       cd ${maindir}/${patch_list[$i]}; git apply ${path_patch}/${patch_list[$(expr $i+1)]}/${patch_list[$(expr $i+2)]};
       echo "patch for ${patch_list[$i]} success"
    done
}

apply_rk3399_patch

echo "++++++++++++++++++++++++++++++++++++++++"
echo "apply_rk3399_patch success"
