# device_board_iSoftStone

## 开发板简介

- 扬帆富设备开发板

“扬帆富设备开发板”（以下简称“扬帆开发板”）搭载瑞芯微RK3399芯片（双 Cortex-A72 大核+四 Cortex-A53 小核）六核 64 位超强 CPU，基于Big.Little架构，搭载 Openharmony 系统，主频最高达 1.8 GHz。采用 Mali-T860 GPU，支持 4K、H265/H264 视频解码。多路视频输出和输入，性能更强，速度更快，接口更丰富，其丰富的扩展接口可实现LCD显示、触摸、多媒体、上网等基本特性，可广泛应用于互动广告机、互动数字标牌、智能自助终端、智能零售终端、工控主机、机器人设备等各类场景。

![图1 扬帆开发板](yangfan/figures/yangfan_board.png)


## 目录

```
device/board/isoftstone
├── yangfan                               # yangfan board
```

## 使用说明

扬帆 参考:
- [yangfan](https://gitee.com/openharmony/device_board_isoftstone/tree/master/yangfan/README_zh.md)


## 相关仓

* [vendor_isoftstone](https://gitee.com/openharmony/vendor_isoftstone)
* [device_soc_rockchip](https://gitee.com/openharmony/device_soc_rockchip)
